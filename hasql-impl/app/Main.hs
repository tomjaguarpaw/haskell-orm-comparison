{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Hasql.Connection as Hasql
import qualified Hasql.Session as Hasql

import Lib

connstring :: Hasql.Settings
connstring = Hasql.settings
  "localhost"  -- host
  5432         -- port
  "postgres"    -- user
  ""           -- password
  "hitmen"     -- dbname

main :: IO ()
main = do
  (Right conn) <- Hasql.acquire connstring
  latestHits <- Hasql.run (Hasql.statement () latestHits) conn
  totalBountyFor1 <- Hasql.run (Hasql.statement (HitmanID 1) totalBountyAwarded) conn

  putStrLn "===== LATEST HITS ====="
  print latestHits

  putStrLn "===== TOTAL BOUNTY FOR HITMAN 1 ====="
  print totalBountyFor1

  Hasql.release conn
