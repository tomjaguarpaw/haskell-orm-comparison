{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

-- Thoughts on Hasql:
--
-- 1) While writing this, I realised that Hasql isn't meant to be a full-fledged
--    DB framework/ORM. Instead, it's meant to be a fast connection layer to
--    Postgres. It's not trying to compete with Beam and Opaleye, it's trying
--    to compete with postgresql-simple. You want your framework to *use*
--    Hasql as a backend. It's solving for a very different set of constraints;
--    where Beam and friends are trying to solve for query *safety and composability*,
--    Hasql is trying to solve for query *speed*.
-- 2) Hasql's use of contravariant functors for interpolating query parameters
--    is slightly annoying and feels unnecessary.
-- 3) One good thing is that unlike most of the other options, Hasql's interface
--    is completely exception free! Everything is signalled through sum types.
--    It also has functionality to handle queries that should only return
--    a single row, so that you don't have to do that handling yourself.
-- 4) There's no type checking at all on your queries, so it's up to you
--    to ensure that your queries won't break. You have to manually keep
--    track of whether a return column should be nonnull, what type it is
--    so that you decode it correctly, and so on. Again, since Hasql is
--    meant to be a *backend* for some framework, your framework should
--    be the one handling it for you. Doing it manually is way too
--    error-prone.
-- 5) It doesn't look like there's an easy way to stream over the results
--    of a query and produce side effects for each element in constant
--    space?

module Lib where

import Data.Int ( Int32, Int64 )
import Data.Time
import Data.Text ( Text )
import Data.Vector ( Vector )
import Data.Function ( (&) )
import Data.Functor.Contravariant ( (>$<) )

import Hasql.Statement as Statement
import qualified Hasql.Encoders as Encoders
import qualified Hasql.Decoders as Decoders

import Text.RawString.QQ ( r )

newtype HandlerID = HandlerID Int32
  deriving (Eq, Show)

newtype HitmanID = HitmanID Int32
  deriving (Eq, Show)

newtype MarkID = MarkID Int32
  deriving (Eq, Show)

data Handler = Handler
  { handlerID :: HandlerID
  , handlerCodename :: Text
  , handlerCreatedAt :: UTCTime
  , handlerUpdatedAt :: UTCTime
  }
  deriving (Eq, Show)

data Hitman = Hitman
  { hitmanID :: HitmanID
  , hitmanCodename :: Text
  , hitmanHandlerID :: HandlerID
  , hitmanCreatedAt :: UTCTime
  , hitmanUpdatedAt :: UTCTime
  }
  deriving (Eq, Show)

data Mark = Mark
  { markID :: MarkID
  , markListBounty :: Int64
  , markFirstName :: Text
  , markLastName :: Text
  , markDescription :: Maybe Text
  , markCreatedAt :: UTCTime
  , markUpdatedAt :: UTCTime
  }
  deriving (Eq, Show)

data PursuingMark = PursuingMark
  { pursuingMarkHitmanID :: HitmanID
  , pursuingMarkMarkID :: MarkID
  , pursuingMarkCreatedAt :: UTCTime
  , pursuingMarkUpdatedAt :: UTCTime
  }
  deriving (Eq, Show)

data ErasedMark = ErasedMark
  { erasedMarkHitmanID :: HitmanID
  , erasedMarkMarkID :: MarkID
  , erasedMarkAwardedBounty :: Int64
  , erasedMarkCreatedAt :: UTCTime
  , erasedMarkUpdatedAt :: UTCTime
  }
  deriving (Eq, Show)

withTimestamps :: Decoders.Row (UTCTime -> UTCTime -> a)
               -> Decoders.Row a
withTimestamps decoder = decoder
  <*> Decoders.column (Decoders.nonNullable Decoders.timestamptz)
  <*> Decoders.column (Decoders.nonNullable Decoders.timestamptz)

hitmanDecoder :: Decoders.Row Hitman
hitmanDecoder = Hitman
  <$> HitmanID `fmap` (Decoders.column (Decoders.nonNullable Decoders.int4))
  <*> Decoders.column (Decoders.nonNullable Decoders.text)
  <*> HandlerID `fmap` (Decoders.column (Decoders.nonNullable Decoders.int4))
    & withTimestamps

markDecoder :: Decoders.Row Mark
markDecoder = Mark
  <$> MarkID `fmap` Decoders.column (Decoders.nonNullable Decoders.int4)
  <*> Decoders.column (Decoders.nonNullable Decoders.int8)
  <*> Decoders.column (Decoders.nonNullable Decoders.text)
  <*> Decoders.column (Decoders.nonNullable Decoders.text)
  <*> Decoders.column (Decoders.nullable Decoders.text)
    & withTimestamps

-- |
-- It doesn't seem like there's an easy way to define this decoder
-- in terms of the original? Except maybe by rewriting the original
-- datatypes using @barbies@.
maybeMarkDecoder :: Decoders.Row (Maybe Mark)
maybeMarkDecoder = do
  id <- (fmap . fmap) MarkID (Decoders.column (Decoders.nullable Decoders.int4))
  listBounty <- Decoders.column (Decoders.nullable Decoders.int8)
  firstName <- Decoders.column (Decoders.nullable Decoders.text)
  lastName <- Decoders.column (Decoders.nullable Decoders.text)
  description <- Decoders.column (Decoders.nullable Decoders.text)
  createdAt <- Decoders.column (Decoders.nullable Decoders.timestamptz)
  updatedAt <- Decoders.column (Decoders.nullable Decoders.timestamptz)
  pure $ Mark
    <$> id
    <*> listBounty
    <*> firstName
    <*> lastName
    <*> pure description
    <*> createdAt
    <*> updatedAt

allHitmen :: Statement () (Vector Hitman)
allHitmen = Statement sql encoder decoder True
  where sql = "SELECT h.id, h.codename, h.handler_id, h.created_at, h.updated_at FROM hitmen as h;"
        encoder = Encoders.noParams
        decoder = Decoders.rowVector hitmanDecoder

activelyPursuingMarks :: Statement () (Vector Hitman)
activelyPursuingMarks = Statement sql encoder decoder True
  where sql = [r|
                WITH has_pursuing AS
                  (SELECT h.id AS hitman_id, pm.mark_id
                     FROM hitmen AS h,
                          pursuing_marks AS pm
                    WHERE h.id = pm.hitman_id),
                     active_marks AS
                  (SELECT DISTINCT hp.hitman_id
                     FROM has_pursuing AS hp
                     LEFT OUTER JOIN erased_marks AS em
                       ON hp.mark_id = em.mark_id
                    WHERE em.mark_id IS NULL)
                SELECT h.id, h.codename, h.handler_id, h.created_at, h.updated_at
                  FROM hitmen AS h,
                       active_marks AS am
                 WHERE h.id = am.hitman_id;
              |]
        encoder = Encoders.noParams
        decoder = Decoders.rowVector hitmanDecoder

erasedSince :: Statement UTCTime (Vector Mark)
erasedSince = Statement sql encoder decoder True
  where sql = [r|
                SELECT m.id, m.list_bounty, m.first_name, m.last_name, m.description, m.created_at, m.updated_at
                  FROM marks AS m,
                       erased_marks AS em
                 WHERE m.id = em.mark_id
                   AND em.created_at >= $1;
              |]
        encoder = Encoders.param (Encoders.nonNullable Encoders.timestamptz)
        decoder = Decoders.rowVector markDecoder

-- Since Hasql isn't really meant to be an ORM/DB framework, we have to
-- just copy and paste the raw SQL. We could reuse `erasedSince` by using
-- a `Session` instead of `Statement`, but that seems against the intended usage.
erasedSinceBy :: Statement (UTCTime, HitmanID) (Vector Mark)
erasedSinceBy = Statement sql encoder decoder True
  where sql = [r|
                SELECT m.id, m.list_bounty, m.first_name, m.last_name, m.description, m.created_at, m.updated_at
                  FROM marks AS m,
                       erased_marks AS em
                 WHERE m.id = em.mark_id
                   AND em.created_at >= $1
                   AND em.hitman_id = $2;
              |]
        encoder = (fst >$< Encoders.param (Encoders.nonNullable Encoders.timestamptz)) <>
                  ((\(HitmanID id) -> id) . snd >$< Encoders.param (Encoders.nonNullable Encoders.int4))
        decoder = Decoders.rowVector markDecoder

totalBountiesAwarded :: Statement () (Vector (Hitman, Int64))
totalBountiesAwarded = Statement sql encoder decoder True
  where sql = [r|
                WITH awarded AS
                  (SELECT h.id AS hitman_id, COALESCE(SUM(em.awarded_bounty), 0) AS total
                     FROM hitmen AS h
                     LEFT OUTER JOIN erased_marks AS em
                       ON h.id = em.hitman_id
                    GROUP BY h.id)
                SELECT h.id, h.codename, h.handler_id, h.created_at, h.updated_at, CAST(awarded.total AS BIGINT)
                  FROM hitmen AS h,
                       awarded
                  WHERE h.id = awarded.hitman_id;
              |]
        encoder = Encoders.noParams
        decoder = Decoders.rowVector $ (,)
          <$> hitmanDecoder
          <*> Decoders.column (Decoders.nonNullable Decoders.int8)

totalBountyAwarded :: Statement HitmanID Int64
totalBountyAwarded = Statement sql encoder decoder True
  where sql = [r|
                WITH awarded AS
                  (SELECT h.id AS hitman_id, COALESCE(SUM(em.awarded_bounty), 0) AS total
                     FROM hitmen AS h
                     LEFT OUTER JOIN erased_marks AS em
                       ON h.id = em.hitman_id
                    GROUP BY h.id)
                SELECT CAST(awarded.total AS BIGINT)
                   FROM awarded
                  WHERE awarded.hitman_id = $1;
              |]
        encoder = (\(HitmanID id) -> id) >$< Encoders.param (Encoders.nonNullable Encoders.int4)
        decoder = Decoders.singleRow $
          Decoders.column (Decoders.nonNullable Decoders.int8)

latestHits :: Statement () (Vector (Hitman, Maybe Mark))
latestHits = Statement sql encoder decoder True
  where sql = [r|
                WITH max_dates AS
                  (SELECT em.hitman_id, MAX(em.created_at) AS max_date
                     FROM erased_marks AS em
                    GROUP BY em.hitman_id),
                     min_marks_by_date AS
                  (SELECT em.hitman_id, em.created_at, MIN(em.mark_id) AS min_mark_id
                     FROM erased_marks AS em
                    GROUP BY em.hitman_id, em.created_at)
                SELECT h.*, m.*
                  FROM hitmen AS h
                  LEFT OUTER JOIN max_dates AS maxd
                    ON h.id = maxd.hitman_id
                  LEFT OUTER JOIN min_marks_by_date AS minmks
                    ON maxd.hitman_id = minmks.hitman_id
                   AND maxd.max_date = minmks.created_at
                  LEFT OUTER JOIN marks AS m
                    ON m.id = minmks.min_mark_id;
              |]
        encoder = Encoders.noParams
        decoder = Decoders.rowVector $ (,)
          <$> hitmanDecoder
          <*> maybeMarkDecoder

latestHit :: Statement HitmanID (Maybe Mark)
latestHit = Statement sql encoder decoder True
  where sql = [r|
                WITH max_dates AS
                  (SELECT em.hitman_id, MAX(em.created_at) AS max_date
                     FROM erased_marks AS em
                    GROUP BY em.hitman_id),
                     min_marks_by_date AS
                  (SELECT em.hitman_id, em.created_at, MIN(em.mark_id) AS min_mark_id
                     FROM erased_marks AS em
                    GROUP BY em.hitman_id, em.created_at)
                SELECT m.*
                  FROM hitmen AS h
                  LEFT OUTER JOIN max_dates AS maxd
                    ON h.id = maxd.hitman_id
                  LEFT OUTER JOIN min_marks_by_date AS minmks
                    ON maxd.hitman_id = minmks.hitman_id
                   AND maxd.max_date = minmks.created_at
                  LEFT OUTER JOIN marks AS m
                    ON m.id = minmks.min_mark_id
                 WHERE h.id = $1;
              |]
        encoder = (\(HitmanID id) -> id) >$< Encoders.param (Encoders.nonNullable Encoders.int4)
        decoder = Decoders.singleRow maybeMarkDecoder

singularPursuer :: Statement () (Vector (Hitman, Mark))
singularPursuer = Statement sql encoder decoder True
  where sql = [r|
                WITH active_marks AS
                  (SELECT m.id AS mark_id
                     FROM marks AS m
                     LEFT OUTER JOIN erased_marks AS em
                       ON m.id = em.mark_id
                    WHERE em.mark_id IS NULL),
                     pursuing_counts AS
                  (SELECT m.id AS mark_id, COUNT(pm.hitman_id) AS num_pursuing
                     FROM marks AS m,
                          pursuing_marks AS pm
                    WHERE m.id = pm.mark_id
                    GROUP BY m.id)
                SELECT h.*, m.*
                  FROM marks AS m,
                       active_marks AS active,
                       pursuing_counts AS has_pursuing,
                       pursuing_marks AS pm,
                       hitmen AS h
                 WHERE m.id = active.mark_id
                   AND m.id = has_pursuing.mark_id
                   AND m.id = pm.mark_id
                   AND h.id = pm.hitman_id
                   AND has_pursuing.num_pursuing = 1;
              |]
        encoder = Encoders.noParams
        decoder = Decoders.rowVector $ (,)
          <$> hitmanDecoder
          <*> markDecoder

marksOfOpportunity :: Statement () (Vector (Hitman, Mark))
marksOfOpportunity = Statement sql encoder decoder True
  where sql = [r|
                WITH no_pursuing AS
                  (SELECT em.hitman_id, em.mark_id
                     FROM erased_marks AS em
                     LEFT OUTER JOIN pursuing_marks AS pm
                       ON em.mark_id = pm.mark_id
                      AND em.hitman_id = pm.hitman_id
                    WHERE pm.hitman_id IS NULL)
                SELECT h.*, m.*
                  FROM hitmen AS h,
                       marks AS m,
                       no_pursuing AS np
                 WHERE h.id = np.hitman_id
                   AND m.id = np.mark_id;
              |]
        encoder = Encoders.noParams
        decoder = Decoders.rowVector $ (,)
          <$> hitmanDecoder
          <*> markDecoder
